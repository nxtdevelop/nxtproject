import { BrowserRouter, Routes, Route } from "react-router-dom"
import { PrivateRoute, AdminPrivateRoute } from "./components/PrivateRoute"
import Layout from "./components/Layout"

import HomePage from "./components/pages/HomePage"
import LoginPage from "./components/pages/LoginPage"
import RegisterPage from "./components/pages/RegisterPage"
import SoloProduct from "./components/pages/SoloProduct"
import CatePage from "./components/pages/CatePage"

import AdminPage from "./components/pages/AdminPage"
import AddProductPage from "./components/pages/AddProductPage"
import EditProductPage from "./components/pages/EditProductPage"
import SearchByCate from "./components/pages/SearchByCate"
import CartPage from "./components/pages/CartPage"
import UserProfile from "./components/pages/UserProfile"
import SoloOrder from "./components/pages/SoloOrder"


function App() {

  return (
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<Layout />} >
                <Route path="login" element={<LoginPage />} />
                <Route path="register" element={<RegisterPage />} />

                <Route index element={<HomePage />} />
                <Route path="product/:slug" element={<SoloProduct />} />

                <Route path="cate" element={<CatePage />} />
                <Route path="cate/:cate" element={<SearchByCate />} />

                <Route element={<PrivateRoute />} >
                    <Route path="cart" element={<CartPage />} />
                    <Route path="profile" element={<UserProfile />} />
                    <Route path="order/:id" element={<SoloOrder />} />
                </Route>

                <Route path="admin" element={<AdminPrivateRoute />} >
                    <Route index element={<AdminPage />} />
                    <Route path="add" element={<AddProductPage />} />
                    <Route path="edit/:id" element={<EditProductPage />} />
                </Route>

            </Route>
        </Routes>
    </BrowserRouter>
  )
}

export default App
